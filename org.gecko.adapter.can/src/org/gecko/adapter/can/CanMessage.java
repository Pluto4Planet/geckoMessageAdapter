/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.can;

import java.util.Date;

import org.gecko.osgi.messaging.Message;

/**
 * A can specific message
 * @author Mark Hoffmann
 * @since 17.09.2018
 */
public interface CanMessage extends Message {

	/**
	 * Returns the message time-stamp
	 * @return the message time-stamp
	 */
	public Date getTimestamp();
}
