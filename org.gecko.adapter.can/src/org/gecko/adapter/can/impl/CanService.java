/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.can.impl;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gecko.core.pushstream.PushStreamHelper;
import org.gecko.osgi.messaging.Message;
import org.gecko.osgi.messaging.MessagingConstants;
import org.gecko.osgi.messaging.MessagingContext;
import org.gecko.osgi.messaging.MessagingService;
import org.gecko.osgi.messaging.SimpleMessage;
import org.osgi.annotation.bundle.Capability;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.osgi.util.pushstream.PushEvent;
import org.osgi.util.pushstream.PushStream;
import org.osgi.util.pushstream.PushStreamBuilder;
import org.osgi.util.pushstream.PushStreamProvider;
import org.osgi.util.pushstream.SimplePushEventSource;

import com.github.kayak.core.Bus;
import com.github.kayak.core.BusURL;
import com.github.kayak.core.Frame;
import com.github.kayak.core.FrameListener;
import com.github.kayak.core.Subscription;
import com.github.kayak.core.TimeSource;

/**
 * Can Service implementation
 * @author Mark Hoffmann
 * @since 16.09.2018
 */
@Capability(namespace=MessagingConstants.CAPABILITY_NAMESPACE, name="can.adapter", version="1.0.0", attribute= {"vendor=Gecko.io", "implementation=Process"})
@Component(service=MessagingService.class, name="CanService", configurationPolicy=ConfigurationPolicy.REQUIRE, immediate=true)
public class CanService implements MessagingService, AutoCloseable, FrameListener {

	@ObjectClassDefinition
	@interface CanConfig {

		String candHost() default DEFAULT_CAND_HOST;
		int candPort() default DEFAULT_CAND_PORT;
		String interfaceDevice() default DEFAULT_CAN_DEVICE;
		int objectId() default 1;

	}
	
	private static final Logger logger = Logger.getLogger(CanService.class.getName());
	public static final String DEFAULT_CAN_DEVICE = "can0";
	public static final int DEFAULT_CAND_PORT = 29536;
	public static final String DEFAULT_CAND_HOST = "localhost";
	private PushStreamProvider provider = new PushStreamProvider();
	private Map<Integer, SimplePushEventSource<Message>> subscriptions = new ConcurrentHashMap<>();
	private Bus bus = new Bus();
	private final TimeSource timeSource = new TimeSource();
	private int objectId;
	
	/**
	 * Called on component activation
	 * @param config the configuration for the message adapter
	 * @param context the context object
	 * @throws Exception
	 */
	@Activate	
	void activate(CanConfig config, BundleContext context) throws Exception {
		String device = config.interfaceDevice();
		String host = config.candHost();
		int port = config.candPort();
		objectId  = config.objectId();
		
		BusURL url = new BusURL(host, port, device);
		bus = new Bus();
		bus.setConnection(url);
		bus.setTimeSource(timeSource);
	}
	
	/**
	 * Called on component de-activation
	 * @throws Exception
	 */
	@Deactivate
	void deactivate() throws Exception {
		close();
	}
	
	/* 
	 * (non-Javadoc)
	 * @see java.lang.AutoCloseable#close()
	 */
	@Override
	public void close() throws Exception {
		if (timeSource != null) {
			timeSource.stop();
		}
		if (bus != null) {
			String device = bus.getConnection().getBus();
			bus.disconnect();
			bus.destroy();
			logger.log(Level.INFO, "Stopped CAN bus '{0}'", device);
		}
		subscriptions.forEach((id, evts)->evts.close());
		subscriptions.clear();
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.osgi.messaging.MessagingService#subscribe(java.lang.String)
	 */
	@Override
	public PushStream<Message> subscribe(String topic) throws Exception {
		return subscribe(topic, null);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.osgi.messaging.MessagingService#subscribe(java.lang.String, org.gecko.osgi.messaging.MessagingContext)
	 */
	@Override
	public PushStream<Message> subscribe(String topic, MessagingContext context) throws Exception {
		startTimeSource();
		try {
			int subscribeId = parseObjectId(topic, objectId);
			SimplePushEventSource<Message> source = subscriptions.get(Integer.valueOf(subscribeId));
			if (source == null) {
				Subscription subscription = new Subscription(this, bus);
				subscription.subscribe(subscribeId,  false);
				bus.addSubscription(subscription);
				source = provider.buildSimpleEventSource(Message.class).build();
				subscriptions.put(Integer.valueOf(subscribeId), source);
			}
			PushStreamBuilder<Message,BlockingQueue<PushEvent<? extends Message>>> buildStream = PushStreamHelper.configurePushStreamBuilder(source, context);
			return buildStream.build();
		} catch(Exception e){
			throw new Exception(e.getMessage(), e);
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.osgi.messaging.MessagingService#publish(java.lang.String, java.nio.ByteBuffer)
	 */
	@Override
	public void publish(String topic, ByteBuffer content) throws Exception {
		publish(topic, content, null);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.osgi.messaging.MessagingService#publish(java.lang.String, java.nio.ByteBuffer, org.gecko.osgi.messaging.MessagingContext)
	 */
	@Override
	public void publish(String topic, ByteBuffer content, MessagingContext context) throws Exception {
		startTimeSource();
		if (content.array().length > 8) {
			throw new IllegalArgumentException("Content for the can bus must be at most 8 byte long");
		}
		int subscribeId = parseObjectId(topic, objectId);
		Frame f = new Frame(subscribeId, false, content.array());
		f.setTimestamp(timeSource.getTime());
		bus.sendFrame(f);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see com.github.kayak.core.FrameListener#newFrame(com.github.kayak.core.Frame)
	 */
	@Override
	public void newFrame(Frame frame) {
		Iterator<Entry<Integer, SimplePushEventSource<Message>>> it = subscriptions.entrySet().iterator();
		while(it.hasNext()){
			Entry<Integer, SimplePushEventSource<Message>> e = it.next();
			if(!e.getKey().equals(frame.getIdentifier())){
				continue;
			}
			SimplePushEventSource<Message> source = e.getValue();
			if(!source.isConnected()){
				source.close();
				it.remove();
			} else {
				try {
					Message msg = fromCanMessage(frame);
					source.publish(msg);
				} catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Starts the time source 
	 */
	private void startTimeSource() {
		if (!timeSource.getMode().equals(TimeSource.Mode.PLAY)) {
			timeSource.play();
		}
	}
	
	/**
	 * Tries to parse the topic into a client id
	 * @param topic the topic
	 * @param defaultId the default client id as fallback
	 * @return the can message client id
	 */
	private static int parseObjectId(String topic, int defaultId) {
		int subscribeId = defaultId;
		try {
			subscribeId = Integer.parseInt(topic);
		} catch (NumberFormatException e) {
			subscribeId = defaultId;
			logger.warning("Cannot parse the topic to a integer, ignoring it and taking default client id " + defaultId);
		}
		return subscribeId;
	}
	
	/**
	 * @param frame
	 * @return
	 */
	private static Message fromCanMessage(Frame frame) {
		ByteBuffer content = ByteBuffer.wrap(frame.getData());
		return new SimpleMessage(Integer.valueOf(frame.getIdentifier()).toString(), content);
	}

}
