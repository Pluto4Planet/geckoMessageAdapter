package org.eclipselabs.ws.messaging.servlet;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;

public class EventSocket extends WebSocketAdapter {
	
	private AtomicReference<Session> session = new AtomicReference<Session>(null);
	private Timer timer = new Timer("msg-timer");
	private volatile int cnt = 0;
	private TimerTask task = new TimerTask() {
		
		@Override
		public void run() {
			if (session.get() != null) {
				cnt++;
				try {
					System.out.println("Server send: " + cnt);
					if (session.get() != null && session.get().isOpen()) {
						session.get().getRemote().sendString("Message-" + cnt);
					} else {
						System.out.println("Session is closed");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	};
	
    @Override
    public void onWebSocketConnect(Session sess) {
        super.onWebSocketConnect(sess);
        session.set(sess);
        System.out.println("Socket Connected: " + sess);
    }
    
    @Override
    public void onWebSocketText(String message) {
        super.onWebSocketText(message);
        System.out.println("Received TEXT message: " + message);
        if (message.equalsIgnoreCase("start") && session != null) {
        	System.out.println("Starting timer");
        	timer.schedule(task, 10l, 1500l);
        }
        if (message.equalsIgnoreCase("stop") && session != null) {
        	System.out.println("Stopping timer");
        	timer.cancel();
        }
    }
    
    @Override
    public void onWebSocketBinary(byte[] payload, int offset, int len) {
    	super.onWebSocketBinary(payload, offset, len);
    	String string = new String(payload);
        System.out.println("Received BIN message: with length " + len + " [" + string + "]");
        if (string.equalsIgnoreCase("start") && session != null) {
        	System.out.println("Starting timer");
        	timer.schedule(task, 10l, 1500l);
        }
        if (string.equalsIgnoreCase("stop") && session != null) {
        	System.out.println("Stopping timer");
        	timer.cancel();
        }
    }
    
    @Override
    public void onWebSocketClose(int statusCode, String reason)  {
        super.onWebSocketClose(statusCode,reason);
        if (session.get() != null) {
        	session.get().close();
        }
        session.set(null);
        System.out.println("Socket Closed: [" + statusCode + "] " + reason);
    }
    
    @Override
    public void onWebSocketError(Throwable cause) {
        super.onWebSocketError(cause);
        cause.printStackTrace(System.err);
    }
}