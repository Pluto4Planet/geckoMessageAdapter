/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.ws.session;

import org.eclipse.jetty.websocket.api.Session;
import org.gecko.adapter.ws.WebSocketCloseListener;
import org.gecko.osgi.messaging.Message;
import org.osgi.util.pushstream.SimplePushEventSource;

/**
 * Holder for the web socket session stuff
 * @author Mark Hoffmann
 * @since 11.10.2017
 */
public class WebSocketSessionHolder {
	
	public WebSocketSessionHolder(String url, WebSocketCloseListener closeListener) {
		this.url = url;
		this.closeListener = closeListener;
	}
	
	public String url;
	public Session session;
	public WebSocketSessionAdapter adapter;
	public WebSocketCloseListener closeListener;
	public SimplePushEventSource<Message> eventSource;

}
