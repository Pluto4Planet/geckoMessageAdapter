/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.ws.session;

import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.gecko.osgi.messaging.Message;
import org.gecko.osgi.messaging.SimpleMessage;
import org.osgi.util.pushstream.SimplePushEventSource;

/**
 * Session based {@link WebSocketAdapter}
 * @author Mark Hoffmann
 * @since 11.10.2017
 */
public class WebSocketSessionAdapter extends WebSocketAdapter {

	private static final Logger logger = Logger.getLogger("websocket.adapter");
	private final WebSocketSessionHolder holder;

	public WebSocketSessionAdapter(WebSocketSessionHolder holder) {
		this.holder = holder;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.jetty.websocket.api.WebSocketAdapter#onWebSocketClose(int, java.lang.String)
	 */
	@Override
	public void onWebSocketClose(int statusCode, String reason) {
		super.onWebSocketClose(statusCode, reason);
//		logger.info("[" + holder.url + "] WebSocket closed " + reason);
		holder.closeListener.onClose(holder.url);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.jetty.websocket.api.WebSocketAdapter#onWebSocketConnect(org.eclipse.jetty.websocket.api.Session)
	 */
	@Override
	public void onWebSocketConnect(Session session) {
		super.onWebSocketConnect(session);
//		logger.info("[" + holder.url + "] WebSocket connected " + this);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.jetty.websocket.api.WebSocketAdapter#onWebSocketError(java.lang.Throwable)
	 */
	@Override
	public void onWebSocketError(Throwable error) {
		super.onWebSocketError(error);
//		logger.info("[" + holder.url + "] WebSocket error " + error.getMessage());
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.jetty.websocket.api.WebSocketAdapter#onWebSocketText(java.lang.String)
	 */
	@Override
	public void onWebSocketText(String message) {
		super.onWebSocketText(message);
//		logger.info("[" + holder.url + "] WebSocket onMessage " + message + " [" + this);
		String url = holder.url;
		SimplePushEventSource<Message> source = holder.eventSource;
		if(source.isConnected()){
			try {
				Message msg = new SimpleMessage(url, ByteBuffer.wrap(message.getBytes()));
				source.publish(msg);
			} catch(Exception ex){
				logger.log(Level.SEVERE, "Detected error onWebSocketText", ex);
			}
		}
	}
}
