/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.ws;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.gecko.adapter.ws.session.WebSocketSessionAdapter;
import org.gecko.adapter.ws.session.WebSocketSessionHolder;
import org.gecko.osgi.messaging.Message;
import org.gecko.osgi.messaging.MessagingContext;
import org.gecko.osgi.messaging.MessagingService;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.osgi.util.pushstream.PushStream;
import org.osgi.util.pushstream.PushStreamProvider;
import org.osgi.util.pushstream.SimplePushEventSource;

/**
 * Websocket implementation of the messaging service
 * @author Mark Hoffmann
 * @since 11.10.2017
 */
@Component(service=MessagingService.class, name="WSService", configurationPolicy=ConfigurationPolicy.REQUIRE, immediate=true)
public class WebsocketService implements MessagingService, AutoCloseable, WebSocketCloseListener {
	
	private volatile WebSocketClient client = new WebSocketClient();
	private volatile Map<String, WebSocketSessionHolder> sessionMap = new ConcurrentHashMap<String, WebSocketSessionHolder>();
	private PushStreamProvider provider = new PushStreamProvider();
	private volatile String wsHost;
	
	@ObjectClassDefinition
	@interface WsConfig {

		String brokerUrl();

	}	
	
	/**
	 * Called on component activation
	 * @param config the ws config
	 * @param context the bundle context
	 * @throws Exception
	 */
	@Activate	
	void activate(WsConfig config, BundleContext context) throws Exception {
		wsHost = config.brokerUrl();
	}

	/**
	 * Called on component deactivation
	 * @throws Exception
	 */
	@Deactivate
	void deactivate() throws Exception {
		close();
	}

	@Override
	public PushStream<Message> subscribe(String topic) throws Exception {
		WebSocketSessionHolder sessionHolder = createSession(topic);
		return provider.createStream(sessionHolder.eventSource);
	}

	@Override
	public PushStream<Message> subscribe(String topic, MessagingContext context) throws Exception {
		return subscribe(topic);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.osgi.messaging.MessagingService#publish(java.lang.String, java.nio.ByteBuffer)
	 */
	@Override
	public void publish(String topic, ByteBuffer content) throws Exception {
		publish(topic, content, null);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.osgi.messaging.MessagingService#publish(java.lang.String, java.nio.ByteBuffer, org.gecko.osgi.messaging.MessagingContext)
	 */
	@Override
	public void publish(String topic, ByteBuffer content, MessagingContext context) throws Exception {
		WebSocketSessionHolder sessionHolder = createSession(topic);
		Session session = sessionHolder.session;
		if (session.isOpen()) {
			session.getRemote().sendBytes(content);
		}
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.adapter.ws.WebSocketCloseListener#onClose(java.lang.String)
	 */
	@Override
	public void onClose(String url) {
		WebSocketSessionHolder sessionHolder = sessionMap.remove(url);
		if (sessionHolder != null && 
				sessionHolder.session != null && 
				sessionHolder.session.isOpen()) {
			sessionHolder.session.close();
		}
		if (sessionHolder.eventSource.isConnected()) {
			sessionHolder.eventSource.endOfStream();
			sessionHolder.eventSource.close();
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see java.lang.AutoCloseable#close()
	 */
	@Override
	public void close() throws Exception {
		sessionMap.values().forEach((s)->{
			if (s.session != null) {
				s.session.close();
			}
		});
		if (client.isRunning()) {
			client.stop();
		}
	}
	
	/**
	 * Creates a websocket session for the given topic, if it does not exist
	 * @param topic the topic / path
	 * @throws Exception
	 */
	private WebSocketSessionHolder createSession(String topic) throws Exception {
		String url = getUrl(topic);
		WebSocketSessionHolder holder = sessionMap.get(url);
		if (holder != null) {
			return holder;
		}
		URI uri = URI.create(url);
		holder = new WebSocketSessionHolder(url, this);
		WebSocketSessionAdapter wsAdapter = new WebSocketSessionAdapter(holder);
		holder.adapter = wsAdapter;
		client.start();
		// Attempt Connect
		Future<Session> fut = client.connect(wsAdapter, uri);
		// Wait for Connect
		Session wsSession = fut.get();
		SimplePushEventSource<Message> source = provider.buildSimpleEventSource(Message.class).build();
		holder.eventSource = source;
		holder.session = wsSession;
		sessionMap.put(url, holder);
		return holder;
	}
	
	/**
	 * Returns the url from the topic
	 * @param topic the topic
	 * @return the url from the topic
	 */
	private String getUrl(String topic) {
		String url = topic;
		if (!topic.startsWith("/")) {
			url = "/" + topic;
		}
		return wsHost + url;
	}

}
