package org.gecko.adapter.ws.tests.server;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.gecko.adapter.ws.tests.servlet.EventClientSocket;

public class EventServer {
	
	private static URI uri = URI.create("ws://localhost:8888/events/");
	private static WebSocketClient client = new WebSocketClient();
	
    public static void main(String[] args) throws InterruptedException  {
    	Runnable serverRunnable = new WSServerRunnable(null);
    	ExecutorService es = Executors.newSingleThreadExecutor();
    	Future<?> serverFuture = es.submit(serverRunnable);
    	
    	Session s = sendText(null, "Hallo");
    	sendText(s, "start");
    	
    	CountDownLatch latch = new CountDownLatch(1);
    	latch.await(10, TimeUnit.SECONDS);

    	sendText(s, "stop");
    	
    	latch = new CountDownLatch(1);
    	latch.await(3, TimeUnit.SECONDS);
    	
    	serverFuture.cancel(true);
    	es.shutdownNow();
    	s.close();
    	try {
			client.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public static Session sendText(Session session, String text) {
    	if (session == null) {
    		try {
				client.start();
				// The socket that receives events
				EventClientSocket socket = new EventClientSocket();
				// Attempt Connect
				Future<Session> fut = client.connect(socket,uri);
				// Wait for Connect
				session = fut.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
    	try {
			session.getRemote().sendString(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return session;
    }
    
}