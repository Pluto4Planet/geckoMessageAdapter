package org.gecko.adapter.ws.tests.servlet;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;

public class EventClientSocket extends WebSocketAdapter {
	
	private List<String> messages = new LinkedList<>();
	
    @Override
    public void onWebSocketConnect(Session sess)
    {
        super.onWebSocketConnect(sess);
        System.out.println("Client Socket Connected: " + sess);
    }
    
    @Override
    public void onWebSocketText(String message)  {
        super.onWebSocketText(message);
        System.out.println("Received Client TEXT message: " + message);
    }
    
    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        super.onWebSocketClose(statusCode,reason);
        System.out.println("Client Socket Closed: [" + statusCode + "] " + reason);
    }
    
    @Override
    public void onWebSocketError(Throwable cause) {
        super.onWebSocketError(cause);
        cause.printStackTrace(System.err);
    }
    
    public List<String> getMessages() {
    	synchronized (messages) {
			return Collections.unmodifiableList(messages);
		}
    }
}