package org.gecko.adapter.ws.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.component.LifeCycle.Listener;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.gecko.adapter.ws.tests.server.WSServerRunnable;
import org.gecko.adapter.ws.tests.servlet.EventClientSocket;
import org.gecko.osgi.messaging.Message;
import org.gecko.osgi.messaging.MessagingConstants;
import org.gecko.osgi.messaging.MessagingService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.pushstream.PushStream;
import org.osgi.util.tracker.ServiceTracker;

@RunWith(MockitoJUnitRunner.class)
public class WebsocketComponentTest {

	private final BundleContext context = FrameworkUtil.getBundle(WebsocketComponentTest.class).getBundleContext();
	private WebSocketClient client = new WebSocketClient();
	private URI uri = URI.create("ws://localhost:8888/events/");
	private URI brokerUri = URI.create("ws://localhost:8888");
	private Future<?> serverFuture;
	private Configuration clientConfig;
	
	@Before
	public void setup() {
//		startWsServer();
	}

	@After
	public void teardown() throws Exception {
//		stopWsServer();
	}
	
	@Test
	public void test() {
		
	}

	/**
	 * Tests publishing a message
	 * @throws Exception
	 */
//	@Test
	public void testSimpleClient() throws Exception {
    	
    	Session session = sendClientText(null, "Hallo");
    	assertNotNull(session);
    	sendClientText(session, "start");
    	
    	CountDownLatch latch = new CountDownLatch(1);
    	latch.await(5, TimeUnit.SECONDS);

    	sendClientText(session, "stop");
    	
    	latch = new CountDownLatch(1);
    	latch.await(3, TimeUnit.SECONDS);
    	
    	client.addLifeCycleListener(new Listener() {
			
			@Override
			public void lifeCycleStopping(LifeCycle arg0) {
				System.out.println("Stopping");
			}
			
			@Override
			public void lifeCycleStopped(LifeCycle arg0) {
				System.out.println("Stopped");
				
			}
			
			@Override
			public void lifeCycleStarting(LifeCycle arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void lifeCycleStarted(LifeCycle arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void lifeCycleFailure(LifeCycle arg0, Throwable arg1) {
				// TODO Auto-generated method stub
				
			}
		});
    	session.close(StatusCode.NORMAL, "done");
    	try {
    		System.out.println("Stop Client ...");
			client.stop();
			client.setStopTimeout(1000l);
			System.out.println("Stopped Client");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	client = null;
    	client = new WebSocketClient();
    	
    	latch = new CountDownLatch(1);
    	latch.await(2, TimeUnit.SECONDS);
    	System.out.println("stopping " + client.isStopping());
    	System.out.println("stopped " + client.isStopped());
    	System.out.println("failed " + client.isFailed());
    	System.out.println("running " + client.isRunning());
    	System.out.println("starting " + client.isStarting());
    	System.out.println("started " + client.isStarted());
    	System.out.println("Test finished");
    	
    	Session old = session;
    	session = sendClientText(null, "Hallo nochmal");
    	assertNotEquals(old, session);
    	assertNotNull(session);
    	sendClientText(session, "start");
    	
    	latch = new CountDownLatch(1);
    	latch.await(5, TimeUnit.SECONDS);

    	sendClientText(session, "stop");
    	
    	latch = new CountDownLatch(1);
    	latch.await(3, TimeUnit.SECONDS);
	}
	
    /**
	 * Tests publishing a message
	 * @throws Exception
	 */
//	@Test
	public void testPublishMessage() throws Exception {
		final CountDownLatch createLatch = new CountDownLatch(1);
		clientConfig = getConfiguration(context, "WSService", createLatch);

		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put(MessagingConstants.PROP_BROKER, brokerUri.toString());

		// count down latch to wait for the message
		CountDownLatch resultLatch = new CountDownLatch(1);
		// holder for the result

		// starting adapter with the given properties
		clientConfig.update(p);

		// check for service
		MessagingService messagingService = getService(MessagingService.class, 30000l);
		assertNotNull(messagingService);
//
//		
//		//send message and wait for the result
		messagingService.publish("/events", ByteBuffer.wrap("start".getBytes()));
		
		PushStream<Message> subscribe = messagingService.subscribe("/events");
		
		subscribe.forEach((m)->System.out.println("M: " + new String(m.payload().array())));
//		// wait and compare the received message
		resultLatch.await(25, TimeUnit.SECONDS);
		//send message and wait for the result
		messagingService.publish("/events", ByteBuffer.wrap("stop".getBytes()));

	}


	/**
	 * Creates a configuration with the configuration admin
	 * @param context the bundle context
	 * @param configId the configuration id
	 * @param createLatch the create latch for waiting
	 * @return the configuration
	 * @throws Exception
	 */
	private Configuration getConfiguration(BundleContext context, String configId, CountDownLatch createLatch) throws Exception {

		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);

		// create MQTT client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(configId, "?");
		assertNotNull(clientConfig);

		return clientConfig;
	}

	
	<T> T getService(Class<T> clazz, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, clazz, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}
	
	<T> T getService(Filter filter, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, filter, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}

	/**
	 * Client side send text call
	 * @param session the client session
	 * @param text the text to send
	 * @return the session
	 */
	Session sendClientText(Session session, String text) {
		if (session == null) {
			try {
				client.start();
				// The socket that receives events
				EventClientSocket socket = new EventClientSocket();
				// Attempt Connect
				Future<Session> fut = client.connect(socket,uri);
				// Wait for Connect
				session = fut.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			session.getRemote().sendString(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return session;
	}
	
	/**
	 * Starts the websocket server
	 */
	void startWsServer() {
		CountDownLatch latch = new CountDownLatch(1);
		Listener lifecycleListener = new Listener() {
			
			@Override
			public void lifeCycleStopping(LifeCycle arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void lifeCycleStopped(LifeCycle arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void lifeCycleStarting(LifeCycle arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void lifeCycleStarted(LifeCycle arg0) {
				latch.countDown();
			}
			
			@Override
			public void lifeCycleFailure(LifeCycle arg0, Throwable arg1) {
				// TODO Auto-generated method stub
				
			}
		};
		Runnable serverRunnable = new WSServerRunnable(lifecycleListener);
    	ExecutorService es = Executors.newSingleThreadExecutor();
    	serverFuture = es.submit(serverRunnable);
    	try {
			latch.await(15, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			fail("WS server did not start");
		}
	}
	
	/**
	 * Stops the websocket server
	 */
	void stopWsServer() {
		if (serverFuture != null) {
			serverFuture.cancel(true);
		}
	}

}
