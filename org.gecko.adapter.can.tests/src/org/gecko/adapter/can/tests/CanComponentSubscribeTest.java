/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.can.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.gecko.osgi.messaging.Message;
import org.gecko.osgi.messaging.MessagingService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.pushstream.PushStream;
import org.osgi.util.tracker.ServiceTracker;

@RunWith(MockitoJUnitRunner.class)
public class CanComponentSubscribeTest {

	private final BundleContext context = FrameworkUtil.getBundle(CanComponentSubscribeTest.class).getBundleContext();
	private Configuration clientConfig;
	private SocketCanDMock canSocket;

	@Before
	public void before() {
//		canSocket = new SocketCanDMock(29536);
//		canSocket.setSendMode(true);
//		canSocket.start();
	}

	@After
	public void after() throws IOException {
//		canSocket.stop();
//		if (clientConfig != null) {
//			clientConfig.delete();
//			clientConfig = null;
//		}
	}
	
	
	@Test
	public void test() {
		
	}

//	@Test
	public void testSubscribeCan() throws Exception {
		final CountDownLatch createLatch = new CountDownLatch(1);
		Configuration clientConfig = getConfiguration(context, "CanService", createLatch);
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put("interfaceDevice", "can0");
		p.put("candHost", "localhost");
		p.put("clientId", "1");
		// starting adapter with the given properties
		clientConfig.update(p);
		createLatch.await(1, TimeUnit.SECONDS);

		// count down latch to wait for the message
		CountDownLatch resultLatch = new CountDownLatch(1);
		// check for service
		MessagingService messagingService = getService(MessagingService.class, 30000l);
		assertNotNull(messagingService);
		PushStream<Message> subscribeStream = messagingService.subscribe("1");
		AtomicReference<String> result = new AtomicReference<>();
		subscribeStream.forEach((msg)->{
			byte[] c = msg.payload().array();
			result.set(new String(c));
			});
		// wait and compare the received message
		resultLatch.await(1, TimeUnit.SECONDS);
		canSocket.sendString("< frame 001 1537262.296 8 48 65 6C 6C 6F >");
		resultLatch.await(1, TimeUnit.SECONDS);
		
		assertEquals("Hello", result.get().trim());
		clientConfig.delete();
		Thread.sleep(5000l);
		
	}

//	@Test
	public void testSubscribeCanWrongObjectId() throws Exception {
		final CountDownLatch createLatch = new CountDownLatch(1);
		Configuration clientConfig = getConfiguration(context, "CanService", createLatch);
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put("interfaceDevice", "can0");
		p.put("candHost", "localhost");
		p.put("clientId", "1");
		// starting adapter with the given properties
		clientConfig.update(p);
		createLatch.await(1, TimeUnit.SECONDS);

		// count down latch to wait for the message
		CountDownLatch resultLatch = new CountDownLatch(1);
		// check for service
		MessagingService messagingService = getService(MessagingService.class, 30000l);
		assertNotNull(messagingService);
		PushStream<Message> subscribeStream = messagingService.subscribe("1");
		AtomicReference<String> result = new AtomicReference<>();
		subscribeStream.forEach((msg)->{
			byte[] c = msg.payload().array();
			result.set(new String(c));
			});
		// wait and compare the received message
		resultLatch.await(1, TimeUnit.SECONDS);
		canSocket.sendString("< frame 002 1537262.296 8 48 65 6C 6C 6F >");
		resultLatch.await(1, TimeUnit.SECONDS);
		assertNull(result.get());
		canSocket.sendString("< frame 001 1537262.296 8 48 65 6C 6C 6F >");
		resultLatch.await(1, TimeUnit.SECONDS);
		assertEquals("Hello", result.get().trim());
		clientConfig.delete();
		Thread.sleep(5000l);
	}

	/**
	 * Creates a configuration with the configuration admin
	 * @param context the bundle context
	 * @param configId the configuration id
	 * @param createLatch the create latch for waiting
	 * @return the configuration
	 * @throws Exception
	 */
	private Configuration getConfiguration(BundleContext context, String configId, CountDownLatch createLatch) throws Exception {

		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);

		// create MQTT client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getFactoryConfiguration(configId, "2", "?");
		assertNotNull(clientConfig);

		return clientConfig;
	}

	<T> T getService(Class<T> clazz, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, clazz, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}

	<T> T getService(Filter filter, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, filter, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}

}