/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.can.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.gecko.osgi.messaging.MessagingService;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;

@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CanComponentPublishTest {

	private final BundleContext context = FrameworkUtil.getBundle(CanComponentPublishTest.class).getBundleContext();
	private SocketCanDMock canSocket;

	@Before
	public void before() {
//		canSocket = new SocketCanDMock(29536);
//		canSocket.start();
	}

	@After
	public void after() throws IOException, InterruptedException {
//		canSocket.stop();
	}
	
	@Test
	public void test() {
		
	}
	
//	@Test(expected=IllegalArgumentException.class)
	public void testPublishCanFail() throws Exception {
		final CountDownLatch createLatch = new CountDownLatch(1);
		Configuration clientConfig = getConfiguration(context, "CanService", createLatch);
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put("interfaceDevice", "can0");
		p.put("candHost", "localhost");
		p.put("objectId", "12");
		// starting adapter with the given properties
		clientConfig.update(p);
		createLatch.await(2, TimeUnit.SECONDS);
		CountDownLatch resultLatch = new CountDownLatch(1);
		// check for service
		MessagingService messagingService = getService(MessagingService.class, 30000l);
		assertNotNull(messagingService);
		ByteBuffer payload = ByteBuffer.wrap("HelloDIM2".getBytes());
		messagingService.publish("1010", payload);
		resultLatch.await(1, TimeUnit.SECONDS);
		assertNull(canSocket.getCheckString());
		clientConfig.delete();
		Thread.sleep(5000l);	}
	
//	@Test
	public void testPublishCanDefaultObjectId() throws Exception {
		final CountDownLatch createLatch = new CountDownLatch(1);
		Configuration clientConfig = getConfiguration(context, "CanService", createLatch);
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put("interfaceDevice", "can0");
		p.put("candHost", "localhost");
		p.put("objectId", "12");
		// starting adapter with the given properties
		clientConfig.update(p);
		createLatch.await(2, TimeUnit.SECONDS);
		CountDownLatch resultLatch = new CountDownLatch(1);
		// check for service
		MessagingService messagingService = getService(MessagingService.class, 30000l);
		assertNotNull(messagingService);
		ByteBuffer payload = ByteBuffer.wrap("Hello".getBytes());
		messagingService.publish(null, payload);
		resultLatch.await(1, TimeUnit.SECONDS);
		assertEquals("< send 00c 5 48 65 6C 6C 6F >", canSocket.getCheckString());
		clientConfig.delete();
		Thread.sleep(5000l);
	}

//	@Test
	public void testPublishCan() throws Exception {
		final CountDownLatch createLatch = new CountDownLatch(1);
		Configuration clientConfig = getConfiguration(context, "CanService", createLatch);
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put("interfaceDevice", "can0");
		p.put("candHost", "localhost");
		p.put("objectId", "1");
		// starting adapter with the given properties
		clientConfig.update(p);
		createLatch.await(2, TimeUnit.SECONDS);
		CountDownLatch resultLatch = new CountDownLatch(1);
		// check for service
		MessagingService messagingService = getService(MessagingService.class, 30000l);
		assertNotNull(messagingService);
		ByteBuffer payload = ByteBuffer.wrap("HelloDIM".getBytes());
		messagingService.publish("1010", payload);
		resultLatch.await(1, TimeUnit.SECONDS);
		assertEquals("< send 3f2 8 48 65 6C 6C 6F 44 49 4D >", canSocket.getCheckString());
		clientConfig.delete();
		Thread.sleep(5000l);
	}
	
	/**
	 * Creates a configuration with the configuration admin
	 * @param context the bundle context
	 * @param configId the configuration id
	 * @param createLatch the create latch for waiting
	 * @return the configuration
	 * @throws Exception
	 */
	private Configuration getConfiguration(BundleContext context, String configId, CountDownLatch createLatch) throws Exception {

		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);

		// create MQTT client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getFactoryConfiguration(configId, "1", "?");
		assertNotNull(clientConfig);

		return clientConfig;
	}

	<T> T getService(Class<T> clazz, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, clazz, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}

	<T> T getService(Filter filter, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, filter, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}

}