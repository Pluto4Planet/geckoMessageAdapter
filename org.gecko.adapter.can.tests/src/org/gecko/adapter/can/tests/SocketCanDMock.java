/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.can.tests;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 
 * @author mark
 * @since 17.09.2018
 */
public class SocketCanDMock implements Runnable {

	private static final int ELEMENT_SIZE = 512;
	private final char[] elementBuffer = new char[ELEMENT_SIZE];
	private int port;
	private ServerSocket server;
	private volatile boolean running = false;
	private AtomicReference<String> check = new AtomicReference<String>();
	private AtomicReference<Socket> socket = new AtomicReference<Socket>();
	private BufferedOutputStream bos;
	private boolean sendMode = false;

	/**
	 * Creates a new instance.
	 */
	public SocketCanDMock(int port) {
		this.port = port;
	}
	
	public void setSendMode(boolean mode) {
		this.sendMode = mode;
	}

	public String getCheckString() {
		return check.get();
	}
	
	public void sendString(String sendString) throws IOException {
		if (sendMode && socket.get() != null) {
			BufferedOutputStream bos = new BufferedOutputStream(socket.get().getOutputStream());
			bos.write(sendString.getBytes());
			bos.flush();
		}
	}

	public void start() {
		if (server == null) {
			try {
				running = true;
				server = new ServerSocket(port);
				server.setSoTimeout(200000);
				Executors.newSingleThreadExecutor().submit(this::run);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		if (server != null) {
			running = false;
			try {
				if (socket.get() != null) {
					socket.get().close();
				}
				server.close();
				server = null;
				check.set(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		if (server == null) {
			return;
		}
		while(running) {
			Socket client = null;
			try {
				client = server.accept();
				bos = new BufferedOutputStream(client.getOutputStream());
				bos.write("< hi >".getBytes());
				bos.flush();
				String v = handleConnection ( client );
				if (v.contains("open")) {
					bos.write("< ok >".getBytes());
					bos.flush();
				}
				if (!sendMode) {
					v = handleConnection ( client );
					check.set(v);
				}
			} catch ( IOException e ) {
				if (running) {
					e.printStackTrace();
				} 
			} catch ( Exception e ) {
				e.printStackTrace();
			} finally {
				if ( client != null ) {
					socket.set(client);
					if (!sendMode) {
						try { 
							client.close(); 
						} catch ( IOException e ) { 
						}
					}
				}
			}
		}
	}

	private String handleConnection( Socket client ) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
		int pos = 0;
		boolean inElement = false;
		while (true) {
			char c = (char) reader.read();
			/* Find opening < */
			if (!inElement) {
				if (c == '<') {
					inElement = true;
					elementBuffer[pos] = c;
					pos++;
				}
			} else {
				if(pos >= ELEMENT_SIZE-1) { /* Handle large elements */
					pos = 0;
					inElement = false;

				} else if (c == '>') { /* Find closing > */
					elementBuffer[pos] = c;
					pos++;
					break;
				} else { /* Element content */
					elementBuffer[pos] = c;
					pos++;
				}
			}
		}
		String value = String.valueOf(elementBuffer, 0, pos);
		return value;
	}


}
